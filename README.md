# Spring Web Reactive Workshop

This repo contains the necessary source files for the Spring Web Reactive Workshop.

The following software is required to do the exercises:

* Java 8 SDK
* Maven / Gradle
* Your favorite IDE
* NodeJS
* Angular-cli

## Workshop part 1: Master the koans

Import the `reactive-spring-intro` project into your favorite IDE.

The goal is to fix all the unit tests in: `reactive-spring-intro/src/test/java/com/jdriven/reactive/exercises`

The recommended order for fixing the tests is:

1. E1_CreateTest
2. E2_FilterTest
3. E3_TransformTest
4. E4_ResilienceTest
5. E5_SpecialsTest (optional)

Useful links:

- [Project Reactor - Flux](https://projectreactor.io/core/docs/api/reactor/core/publisher/Flux.html)
- [Project Reactor - Mono](https://projectreactor.io/core/docs/api/reactor/core/publisher/Mono.html)

## Workshop part 2: Mememon Go

Import the `mememon-go-server` project into your favorite IDE.

We will be making a reactive game, consisting of two parts.

#### Backend

The backend is a Spring Web Reactive application which is partly implemented.

Finish the implementation (which can be verified by executing the tests in: `mememon-go-server/src/test/java/com/jdriven/reactive`).

The recommended order for finishing the implementation is:

1. MememonDao
2. MememonService
3. WorldService
4. MememonController
5. WorldController

To run the application, execute one of the following commands:

- Maven: `mvn spring-boot:run`
- Gradle: `gradle bootRun`
- IDE: Run the main method in `com.jdriven.reactive.MememonGoServerApplication`

This will make the backend application available at: <http://localhost:8080>

#### Frontend

The frontend is an Angular2 application, which is already implemented for your convenience.
To run the frontend application, run the following commands from a command shell inside the `mememon-go-client` project:

```shell
npm install
npm install -g angular-cli
ng serve
```

This will make the frontend application available at: <http://localhost:4200>