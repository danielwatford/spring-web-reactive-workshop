package com.jdriven.reactive.service;

import static java.util.stream.Collectors.toList;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Random;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.google.common.collect.Lists;
import com.jdriven.reactive.domain.Coordinate;
import com.jdriven.reactive.domain.Mememon;
import reactor.core.publisher.Flux;

@RunWith(MockitoJUnitRunner.class)
public class WorldServiceTest {

    @Mock
    private MememonService mememonService;

    @InjectMocks
    private WorldService worldService;

    @Test(timeout = 5_000)
    public void shouldEncounterAllMememonsWhenPassingEnoughLocations() throws InterruptedException {
        List<Mememon> mememons = Lists.newArrayList(
                new Mememon(1L, "test1", "test1", 10),
                new Mememon(2L, "test2", "test2", 20),
                new Mememon(3L, "test3", "test3", 30)
        );

        when(mememonService.getAllMememons()).thenReturn(Flux.fromIterable(mememons));

        List<Long> ids = mememons.stream().map(Mememon::getId).collect(toList());
        worldService.getEncounters().subscribe(id -> {
            if (!ids.remove(id)) {
                Assert.fail("Unexpected id: " + id);
            }
        });

        Random random = new Random();
        while (!ids.isEmpty()) {
            worldService.locationReceived(new Coordinate(random.nextInt(100), random.nextInt(100)));
        }
    }
}