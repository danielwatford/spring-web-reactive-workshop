package com.jdriven.reactive.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import com.google.common.collect.Iterables;
import com.jdriven.reactive.dao.MememonDao;
import com.jdriven.reactive.domain.Mememon;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(MockitoJUnitRunner.class)
public class MememonServiceTest {

    @Mock
    private MememonDao mememonDao;

    @InjectMocks
    private MememonService mememonService;

    @Test
    public void shouldGetAllMememons() {
        Mememon m1 = new Mememon(1L, "test1", "test1", 10);
        Mememon m2 = new Mememon(2L, "test2", "test2", 20);

        when(mememonDao.getAll()).thenReturn(Flux.just(m1, m2));

        StepVerifier.create(mememonService.getAllMememons())
                .expectNext(m1, m2)
                .verifyComplete();
    }

    @Test
    public void shouldGetExistingById() {
        Mememon mememon = new Mememon(123L, "test", "test", 10);
        Mono<Long> id = Mono.just(mememon.getId());
        when(mememonDao.getById(id)).thenReturn(Mono.just(mememon));

        StepVerifier.create(mememonService.getById(id))
                .assertNext(m -> assertEquals(mememon.getId(), m.getId()))
                .verifyComplete();
    }

    @Test
    public void shouldGetEmptyMonoForNonExistingId() {
        Mono<Long> id = Mono.just(123L);
        when(mememonDao.getById(id)).thenReturn(Mono.empty());

        StepVerifier.create(mememonService.getById(id))
                .verifyComplete();
    }

    @Test
    public void shouldBeAbleToCatchWeakMememon() {
        Mememon weakMememon = new Mememon(1L, "weak", "test", 0);

        StepVerifier.create(mememonService.catchMememon(Mono.just(weakMememon)))
                .expectNext(true)
                .verifyComplete();
    }

    @Test
    public void shouldNotBeAbleToCatchStrongMememon() {
        Mememon strongMememon = new Mememon(1L, "strong", "test", 100);

        StepVerifier.create(mememonService.catchMememon(Mono.just(strongMememon)))
                .expectNext(false)
                .verifyComplete();
    }

    @Test
    public void shouldSometimesCatchNormalMememon() {
        Mememon normalMememon = new Mememon(1L, "normal", "test", 50);

        // After 1000 attempts we should have at least succeeded once and failed once (statistically this could fail of course)
        final Iterable<Boolean> returnValues = Flux.range(1, 1000)
                .map(i -> normalMememon)
                .map(Mono::just)
                .flatMap(mememonService::catchMememon)
                .distinct()
                .toIterable();
        assertTrue(Iterables.contains(returnValues, true));
        assertTrue(Iterables.contains(returnValues, false));
    }
}
