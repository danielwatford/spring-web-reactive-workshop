package com.jdriven.reactive.controller;

import static org.springframework.http.CacheControl.noCache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.jdriven.reactive.domain.Coordinate;
import com.jdriven.reactive.service.WorldService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Link these REST endpoints to methods in the {@link WorldService}.
 */
@RestController
@RequestMapping(value = "api")
@CrossOrigin(origins = "http://localhost:4200")
public class WorldController {

    private WorldService worldService;

    @Autowired
    public WorldController(WorldService worldService) {
        this.worldService = worldService;
    }

    /**
     * Get the Mememon encounters (Flux of Mememon IDs) from the {@link WorldService}.
     * @return Flux of Mememon IDs
     */
    @GetMapping(value = "encounters", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Long> encounters() {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Posts a new location, notifying the {@link WorldService} that a location has been received.
     * As a result, an OK response is returned using the {@link #createOkResponse()} convenience method.
     * @param location the received location as a Mono
     * @return An OK response Mono
     */
    @PostMapping(value = "location", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<Void>> postLocation(@RequestBody Mono<Coordinate> location) {
        return Mono.error(new RuntimeException("TODO: implement me!"));
    }

    private ResponseEntity<Void> createOkResponse() {
        return ResponseEntity.ok().cacheControl(noCache()).build();
    }

}
