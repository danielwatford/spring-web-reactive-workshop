package com.jdriven.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MememonGoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MememonGoServerApplication.class, args);
	}
}
