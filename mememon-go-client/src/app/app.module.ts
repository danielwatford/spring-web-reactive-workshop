import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MememonFieldComponent } from './mememon-field/mememon-field.component';
import {MdCoreModule} from "@angular2-material/core";
import {MdButtonModule} from "@angular2-material/button";
import {MdCardModule} from "@angular2-material/card";
import {MdToolbarModule} from "@angular2-material/toolbar";
import 'hammerjs';
import { MemedexComponent } from './memedex/memedex.component';
import {MememonsService} from "./shared/mememons.service";

@NgModule({
  declarations: [
    AppComponent,
    MememonFieldComponent,
    MemedexComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdCoreModule.forRoot(), MdButtonModule.forRoot(), MdCardModule.forRoot(),
    MdToolbarModule.forRoot()
  ],
  providers: [MememonsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
