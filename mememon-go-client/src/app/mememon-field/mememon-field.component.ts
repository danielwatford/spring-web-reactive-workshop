import {Component, OnInit, SkipSelf} from '@angular/core';
import {Observable} from "rxjs";
import {Http, Headers, Response} from "@angular/http";
import {MememonsService} from "../shared/mememons.service";
import {Mememon} from "../shared/mememon";

@Component({
  selector: 'app-mememon-field',
  templateUrl: './mememon-field.component.html',
  styleUrls: ['./mememon-field.component.css'],
  providers: [MememonsService]
})
export class MememonFieldComponent implements OnInit {

  mememonAppeared: boolean;
  catchFailed: boolean;
  mememon: any;

  constructor(private http:Http, @SkipSelf() private mememonsService:MememonsService) { }

  ngOnInit() {
    this.mememonAppeared = false;

    const mememons = Observable.create(observer => {
      const eventSource = new EventSource('http://localhost:8080/api/encounters');
      eventSource.onmessage = x => observer.next(x.data);
      eventSource.onerror = err => {
        // eventSource was probably closed
        eventSource.close();
      };

      return () => {
        eventSource.close();
      };
    });

    var mememonfield = document.getElementById('mememon-field');
    Observable.fromEvent(mememonfield, 'mousemove')
      .filter(() => !this.mememonAppeared)
      .subscribe((e: any) => {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        var data = JSON.stringify({
          x: e.clientX,
          y: e.clientY
        });

        this.http.post('http://localhost:8080/api/location', data, {headers: headers}).subscribe();
    });

    mememons.flatMap(mememon =>  this.http.get('http://localhost:8080/api/mememon/' + mememon).map((res: Response) => res.json()))
      .subscribe(mememon => {
        this.mememonAppeared = true;
        this.mememon = Mememon.fromJS(mememon);
    });
  }

  run() {
    if (this.mememon.strength < 100) {
      this.mememonAppeared = false;
      this.catchFailed = false;
    }
  }

  catchMememon() {
    this.catchFailed = false;
    this.http.post('http://localhost:8080/api/mememon/catch/' + this.mememon.id, {})
      .map((res: Response) => res.json())
      .subscribe(caught => {
        if(caught) {
          this.mememonAppeared = false;
          this.mememonsService.caughtMememon(Mememon.fromJS(this.mememon));
        } else {
          this.catchFailed = true;
        }
    })
  }
}
