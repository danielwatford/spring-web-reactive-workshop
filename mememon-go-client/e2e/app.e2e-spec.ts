import { MememonGoPage } from './app.po';

describe('mememon-go App', function() {
  let page: MememonGoPage;

  beforeEach(() => {
    page = new MememonGoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
