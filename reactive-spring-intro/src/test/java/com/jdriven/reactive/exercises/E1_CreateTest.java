package com.jdriven.reactive.exercises;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;


import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * First set of exercises: creating Fluxes.
 * see: http://projectreactor.io/core/docs/api/reactor/core/publisher/Flux.html
 */
public class E1_CreateTest {

    /**
     * This method returns just the two words "Hello" and "World!"
     *
     * @return Flux of String
     */
    public Flux<String> hello() {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * This methods returns the specified List of String as a Flux
     *
     * @param list List of String
     * @return Flux of String
     */
    public Flux<String> fromList(final List<String> list) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * This method returns an empty Flux
     *
     * @return Flux of String
     */
    public Flux<String> empty() {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * This method returns the specified word [n] times.
     *
     * @param word String word to repeat
     * @param n Integer times to repeat specified word
     * @return Flux of String
     */
    public Flux<String> repeat(final String word, final Integer n) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Create a range of [n] Integers, starting from [start].
     *
     * @param start Integer start of range
     * @param n Integer number of Integers to generate
     * @return Flux of Integer
     */
    public Flux<Integer> range(final Integer start, final Integer n) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //


    @Test
    public void helloTest() {
        StepVerifier.create(hello())
                .expectNext("Hello", "World!")
                .verifyComplete();
    }

    @Test
    public void fromListTest() {
        List<String> list = Arrays.asList("Welcome", "to", "Spring Web Reactive");
        StepVerifier.create(fromList(list))
                .expectNext("Welcome", "to", "Spring Web Reactive")
                .verifyComplete();
    }

    @Test
    public void emptyTest() {
        StepVerifier.create(empty())
                .verifyComplete();
    }

    @Test
    public void repeatTest() {
        StepVerifier.create(repeat("JDriven", 10))
                .expectNext("JDriven")
                .expectNext("JDriven")
                .expectNext("JDriven")
                .expectNext("JDriven")
                .expectNext("JDriven")
                .expectNext("JDriven")
                .expectNext("JDriven")
                .expectNext("JDriven")
                .expectNext("JDriven")
                .expectNext("JDriven")
                .verifyComplete();
    }

    @Test
    public void rangeTest() {
        StepVerifier.create(range(5, 10))
                .expectNext(5, 6, 7, 8, 9, 10, 11, 12, 13, 14)
                .verifyComplete();
    }

}
